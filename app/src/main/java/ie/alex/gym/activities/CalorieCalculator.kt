package ie.alex.gym.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import ie.alex.gym.R
import kotlinx.android.synthetic.main.activity_calorie_calculator.*
import android.widget.Toast.makeText as makeText1

class CalorieCalculator : AppCompatActivity() {

    

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calorie_calculator)

        button2.setOnClickListener {
            textResult.text = "Result:"+ (editNum1.text.toString()+ editNum2.text.toString().toInt()+"Calories")
            makeText1(this, textResult.text, Toast.LENGTH_LONG).show()
        }
    }
}
