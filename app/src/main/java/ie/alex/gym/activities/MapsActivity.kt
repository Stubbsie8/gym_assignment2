package ie.alex.gym.activities

import android.content.pm.PackageManager
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import ie.alex.gym.R
import kotlinx.android.synthetic.main.activity_maps.*

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var Map: GoogleMap
    private lateinit var lastLocation: Location



    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
            return

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }


    override fun onMapReady(googleMap: GoogleMap) {
        Map = googleMap
        setUpMap()



        val Goldstone = LatLng(52.233287, -7.146934)
        Map.addMarker(MarkerOptions()
            .position(Goldstone)
            .title("Goldstone Fitness")
            .snippet("10a, Six Cross Road Business Park, Six Cross Roads Business Park, Kilbarry, Waterford")
            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)).alpha(0.7f))
        Map.moveCamera(CameraUpdateFactory.newLatLngZoom(Goldstone, 13.0f))




        val WaterfordWarriors = LatLng(52.234788, -7.144618)
        Map.addMarker(MarkerOptions().position(WaterfordWarriors).title("Waterford Warriors Strength and Conditioning")
            .snippet("16D, Six Cross Roads Business Park, Outer Ring Rd, Waterford")
            .icon(BitmapDescriptorFactory.defaultMarker(
            BitmapDescriptorFactory.HUE_AZURE)).alpha(0.7f))

        val CillBarra = LatLng(52.239166, -7.141741)
        Map.addMarker(MarkerOptions().position(CillBarra).title("Cill Barra Community Sports Centre")
            .snippet("Saint Saviours Parish Centre, Ballybeg, Waterford")
            .icon(BitmapDescriptorFactory.defaultMarker(
            BitmapDescriptorFactory.HUE_AZURE)).alpha(0.7f))

        val BenDunne = LatLng(52.241596, -7.127345)
        Map.addMarker(MarkerOptions().position(BenDunne).title("Ben Dunne Gym - Waterford")
            .snippet("1, Block 3, Lacken Road Business Park, Waterford")
            .icon(BitmapDescriptorFactory.defaultMarker(
            BitmapDescriptorFactory.HUE_AZURE)).alpha(0.7f))

        val Kingfisher = LatLng(52.244025, -7.121701)
        Map.addMarker(MarkerOptions()
            .position(Kingfisher)
            .title("Kingfisher Club Waterford")
            .snippet("Tramore Rd, Waterford")
            .icon(BitmapDescriptorFactory.defaultMarker(
            BitmapDescriptorFactory.HUE_AZURE)).alpha(0.7f))

        val Crystal = LatLng(52.243499, -7.150412)
        Map.addMarker(MarkerOptions().position(Crystal).title("Crystal Sport & Leisure Centre")
            .snippet("Cork Rd, Waterford")
            .icon(BitmapDescriptorFactory.defaultMarker(
            BitmapDescriptorFactory.HUE_AZURE)).alpha(0.7f))

        val WITArena = LatLng(52.251866, -7.179762)
        Map.addMarker(MarkerOptions().position(WITArena).title("WIT Arena")
            .snippet("Waterford Institute of Technology West Campus, Carriganore, Co. Waterford")
            .icon(BitmapDescriptorFactory.defaultMarker(
            BitmapDescriptorFactory.HUE_AZURE)).alpha(0.7f))

        val Crossfit = LatLng(52.259538, -7.144782)
        Map.addMarker(MarkerOptions().position(Crossfit).title("CrossFit Waterford")
            .snippet("Unit 2 carrickpherish business park, northern industrial estate, cleaboy, road, Waterford")
            .icon(BitmapDescriptorFactory.defaultMarker(
            BitmapDescriptorFactory.HUE_AZURE)).alpha(0.7f))

        val CoreFitness = LatLng(52.258929, -7.112344)
        Map.addMarker(MarkerOptions().position(CoreFitness).title("Core Fitness")
            .snippet("Unit 4, Ballinakill shopping centre, Waterford")
            .icon(BitmapDescriptorFactory.defaultMarker(
            BitmapDescriptorFactory.HUE_AZURE)).alpha(0.7f))

        val ÉnergieFitness = LatLng(52.250196, -7.116572)
        Map.addMarker(MarkerOptions().position(ÉnergieFitness).title("Énergie Fitness Waterford")
            .snippet("Unit 8, Tramore Road Business Park, Waterford")
            .icon(BitmapDescriptorFactory.defaultMarker(
            BitmapDescriptorFactory.HUE_AZURE)).alpha(0.7f))

        val BodyshapePerformance = LatLng(52.264231, -7.120791)
        Map.addMarker(MarkerOptions().position(BodyshapePerformance).title("Body Shape Performance")
            .snippet("28 Mary St, Waterford")
            .icon(BitmapDescriptorFactory.defaultMarker(
            BitmapDescriptorFactory.HUE_AZURE)).alpha(0.7f))

        val Carrickpherish = LatLng(52.261578, -7.146476)
        Map.addMarker(MarkerOptions().position(Carrickpherish).title("Carrickpherish Sports Center Hall")
            .snippet("Waterford")
            .icon(BitmapDescriptorFactory.defaultMarker(
            BitmapDescriptorFactory.HUE_AZURE)).alpha(0.7f))




    }
    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1


    }
}
