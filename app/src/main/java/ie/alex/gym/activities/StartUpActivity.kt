package ie.alex.gym.activities

import android.os.Bundle
import android.content.Intent
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import ie.alex.gym.R
import org.jetbrains.anko.intentFor


class StartUpActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.startupactivity)

        val mStartActBtn = findViewById<Button>(R.id.button)
        val caloryBtn = findViewById<Button>(R.id.calorybtn)
        val locationBtn = findViewById<Button>(R.id.locationbtn)

        mStartActBtn.setOnClickListener {

            startActivity(Intent(this@StartUpActivity, GymListActivity::class.java))
        }

        caloryBtn.setOnClickListener {

            startActivity(Intent(this@StartUpActivity, CalorieCalculator::class.java))//add in calorie page here
        }

        locationBtn.setOnClickListener {

            startActivity (intentFor<MapsActivity>())//add in map page here
        }

}
}