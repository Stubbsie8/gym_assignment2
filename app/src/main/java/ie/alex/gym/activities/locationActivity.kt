package ie.alex.gym.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ie.alex.gym.R
import ie.alex.gym.main.MainApp
import kotlinx.android.synthetic.main.activity_gym.*
import kotlinx.android.synthetic.main.activity_location.*
import org.jetbrains.anko.intentFor


class locationActivity : AppCompatActivity() {

    lateinit var app: MainApp


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)


        gymLocation.setOnClickListener {
            startActivity (intentFor<MapsActivity>())
        }



    }

}
