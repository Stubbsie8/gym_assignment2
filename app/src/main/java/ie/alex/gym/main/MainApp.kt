package ie.alex.gym.main

import android.app.Application
import ie.alex.gym.models.GymJSONStore
import ie.alex.gym.models.GymStore
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info


class MainApp : Application(), AnkoLogger {

    lateinit var gyms: GymStore

    override fun onCreate() {
        super.onCreate()
        gyms = GymJSONStore(applicationContext)
        info("Gym started")
    }
}