package ie.alex.gym.models

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import ie.alex.gym.exists
import ie.alex.gym.read
import ie.alex.gym.write
import org.jetbrains.anko.AnkoLogger
import java.util.*


val JSON_FILE = "gyms.json"
val gsonBuilder = GsonBuilder().setPrettyPrinting().create()
val listType = object : TypeToken<java.util.ArrayList<gymModel>>() {}.type

fun generateRandomId(): Long {
    return Random().nextLong()
}

class GymJSONStore : GymStore, AnkoLogger {

    val context: Context
    var gyms = mutableListOf<gymModel>()

    constructor (context: Context) {
        this.context = context
        if (exists(context, JSON_FILE)) {
            deserialize()
        }
    }

    override fun findAll(): MutableList<gymModel> {
        return gyms
    }

    override fun create(gym: gymModel) {
        gym.id = generateRandomId()
        gyms.add(gym)
        serialize()
    }

    override fun delete(gym: gymModel) {
        gyms.remove(gym)
        serialize()
    }


    override fun update(gym: gymModel) {
        var foundGym: gymModel? = gyms.find{ p -> p.id == gym.id}
        if (foundGym != null) {
            foundGym.title = gym.title
            foundGym.description = gym.description
            foundGym.category = gym.category
            foundGym.category2 = gym.category2
        }
        serialize()
    }

    private fun serialize() {
        val jsonString = gsonBuilder.toJson(gyms, listType)
        write(context, JSON_FILE, jsonString)
    }

    private fun deserialize() {
        val jsonString = read(context, JSON_FILE)
        gyms = Gson().fromJson(jsonString, listType)
    }
}