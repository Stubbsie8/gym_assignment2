package ie.alex.gym.models

import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

var lastId=0L

internal fun getId(): Long{
    return lastId++
}


class GymMemStore : GymStore, AnkoLogger {

    val gyms = ArrayList<gymModel>()

    override fun findAll(): List<gymModel> {
        return gyms
    }

    override fun create(gym: gymModel) {
        gym.id = getId()
        gyms.add(gym)
        logAll()
    }

    override fun update(gym: gymModel) {
        var foundGym: gymModel? = gyms.find {p -> p.id == gym.id}
        if (foundGym != null) {
            foundGym.title = gym.title
            foundGym.description = gym.description
            logAll()
        }
    }

    override fun delete(gym: gymModel) {
        gyms.remove(gym)
        logAll()
    }

    fun logAll() {
        gyms.forEach{ info("${it}") }
    }
}
