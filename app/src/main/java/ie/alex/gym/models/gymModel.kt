package ie.alex.gym.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class gymModel(var id: Long=0,
                    var title: String = "",
                    var description: String = "",
                    var category: String ="",
                    var category2: String ="",
                    var calorie: String = "") : Parcelable




