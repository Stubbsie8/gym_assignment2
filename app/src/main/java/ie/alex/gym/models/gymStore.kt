package ie.alex.gym.models

interface GymStore {
    fun findAll(): List<gymModel>
    fun create(gym: gymModel)
    fun update(gym: gymModel)
    fun delete (gym: gymModel)
}